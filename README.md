# pe_rosa_twist_generator

Node to generate Twist Message from autonomy_msgs:

## Subscriptions

|Topic|Type|
|---|---|
|`NAMESPACE/vehicle/speed`|`automotive_platform_msgs`|
|`NAMESPACE/vehicle/steer`|`automotive_platform_msgs`|

## Publications

|Topic|Type|
|---|---|
|`NAMESPACE/vehicle_motion`|`geometry_msgs/Twist`|

## How to build
Existing workspace:

1. Clone this repo
1. Compile
`cd .. && catkin build`
1. Source
`source devel/setup.bash`
 
### How to launch
`rosrun pe_rosa_twist_generator pe_rosa_twist_generator.py`

### For ROSA setup:
`roslaunch pe_rosa_twist_generator pe_rosa_twist_generator.launch`
