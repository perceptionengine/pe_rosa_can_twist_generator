#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('pe_rosa_twist_generator')

import rospy
import math
import geometry_msgs.msg
import automotive_platform_msgs.msg

import message_filters

class PeRosaTwistGenerator:
    def __init__(self):
        speed_topic = "vehicle/speed"
        steer_topic = "vehicle/steer"
        speed_sub = message_filters.Subscriber(speed_topic, automotive_platform_msgs.msg.Speed)
        steer_sub = message_filters.Subscriber(steer_topic, automotive_platform_msgs.msg.SteerWheel)

        sync_sub = message_filters.ApproximateTimeSynchronizer([speed_sub, steer_sub],
                                                queue_size=5, slop=0.1)
        sync_sub.registerCallback(self.automotive_sync_callback)

        pub_topic = rospy.get_param('~out_topic', 'vehicle_motion')

        self.twist_pub_ = rospy.Publisher(pub_topic, geometry_msgs.msg.TwistStamped, queue_size=1)

        rospy.loginfo("[%s] Listening to %s, %s", rospy.get_name(), speed_topic, steer_topic)
        rospy.loginfo("[%s] Publishing to %s ", rospy.get_name(), pub_topic)

        rospy.spin()


    def automotive_sync_callback(self, speed_msg, steer_msg):
        vehicle_twist = geometry_msgs.msg.TwistStamped()
        vehicle_twist.header = speed_msg.header
        vehicle_twist.twist.linear.x = speed_msg.speed
        vehicle_twist.twist.angular.z = steer_msg.angle
        self.twist_pub_.publish(vehicle_twist)


if __name__ == '__main__':
    rospy.init_node('pe_rosa_twist_generator', anonymous=True)

    node = PeRosaTwistGenerator()
    rospy.spin()
